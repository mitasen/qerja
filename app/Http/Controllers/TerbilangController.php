<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TerbilangController extends Controller
{
    public function index()
    {
        return view('index');
    }
    
   public function terbilang(Request $request)
   {

    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $penyebut = array(
        13 =>"belas",
        12 => "puluh",
        102 => "ratus",
        1002 => "ribu",
        1000002 => "juta",
        1000000002 => "miliar",
        1000000000002 => "triliun");

    $arr_nilai = explode(" ", strtolower($request->input));
    if ($this->cek_invalid($arr_nilai)){
        return json_encode(array(
            'value' => 'invalid',
            'valid' => false
        )); 
    }else{
        $arr_angka = array();
        foreach($arr_nilai as $key => $nilai){
            $arr_jenis = (in_array($nilai, $huruf)) ? $huruf : $penyebut;
            $arr_angka[$key] = array_search($nilai, $arr_jenis);
        }
    
        $sort = $arr_angka;
        rsort($sort);
        $key_besar = array_search($sort[0], $arr_angka);
        $nilai_besar = isset($penyebut[$sort[0]]) ? ($sort[0] - 2) : $sort[0];
        $count_angka = count($arr_angka);
    
        $tempindex = 0;
        $arr_hitung = array();
        
        if($count_angka == 1){
            $total = $arr_angka[0];
        }elseif($count_angka == 2 && $arr_nilai[1] == 'belas'){
            $total = ($arr_angka[0] + 10);
        }else{
            foreach($arr_angka as $key => $val){
                if($key < $key_besar){
                    if(isset($penyebut[$val])){
                        if($penyebut[$val] == 'belas'){
                            $arr_hitung['sebelum'][$tempindex] = (10 + $nilai[$tempindex]) * $nilai_besar;
                        }else{
                            $arr_hitung['sebelum'][$tempindex] = (($val - 2) * $nilai[$tempindex]) * $nilai_besar;
                        }
                        
                    }else{
                        $nilai[$key] = $val;
                        $arr_hitung['sebelum'][$key] = $val * $nilai_besar;
                        $tempindex = $key;
                    }
                }elseif($key != $key_besar){
                    if(isset($penyebut[$val])){
                        if($penyebut[$val] == 'belas'){
                            $arr_hitung['sesudah'][$tempindex] = (10 + $nilai[$tempindex]);
                        }else{
                            $arr_hitung['sesudah'][$tempindex] = (($val - 2) * $nilai[$tempindex]);
                        }

                        // if($key < $key_besar){
                        //     if(isset($penyebut[$arr_angka[($key + 1)]])){ 
                        //         $nilai[$key + 1] = $arr_hitung['sesudah'][$tempindex];
                        //     }
                        // }
                    }else{
                        $nilai[$key] = $val;
                        $arr_hitung['sesudah'][$key] = $val;
                        $tempindex = $key;
                    }
                }
            }
        
            $sebelum = isset($arr_hitung['sebelum']) ? array_sum($arr_hitung['sebelum']) : 0;
            $sesudah = isset($arr_hitung['sesudah']) ? array_sum($arr_hitung['sesudah']) : 0;
        
            $total = $sebelum + $sesudah;
        }
        $arr_hasil = array(
            'value' => number_format($total,0,'.',','),
            'valid' => true
        );
        return json_encode($arr_hasil);
        
    }

    
   }

   public function cek_invalid($inputan){
    $invalid = false;
    $angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $penyebut = array("belas","puluh","ratus","ribu","juta","miliar","triliun");

    $arr_basis_kecil = array("belas", "puluh", "ratus");
    $arr_basis_besar = array("ribu", "juta", "miliar");
    $basis_kecil_included = false;
    $basis_besar_included = false;

    if(in_array($inputan[0], $penyebut)){
        $invalid = true;
    }else{
        foreach($inputan as $key => $val){
            if(!in_array($val, $angka) && !in_array($val, $penyebut)){
                $invalid = true;
            }
    
            if(in_array($val, $penyebut)){
                if((in_array($val, $arr_basis_kecil) && $basis_kecil_included) || (in_array($val, $arr_basis_besar) && $basis_besar_included)){ $invalid = true; }
                if(in_array($val, $arr_basis_kecil)){ $basis_kecil_included = true; }
                if(in_array($val, $arr_basis_besar)){ $basis_besar_included = true; }
            }else{
                $basis_kecil_included = false;
                $basis_besar_included = false;
            }
        }
    }
    
    return $invalid;
}
}
