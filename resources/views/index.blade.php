@extends('layouts.app')

@section('content')
<main role="main">

<section class="jumbotron text-center">
  <div class="container">
  <div class="input-group mb-3">
  <input type="text" class="form-control terbilang" placeholder="Masukkan nilai" aria-label="Masukkan nilai" aria-describedby="basic-addon2">
  <div class="input-group-append">
    <button class="btn btn-outline-secondary tombol" type="button">Button</button>
  </div>
</div>
  <div class="card">
  <div class="card-body angka_show">
    
  </div>
</div>
  </div>
</section>

</main>
   
@endsection

@section('script')

<script>
$(document).ready(function(){
  $(".tombol").click(function(){
    var input = $(".terbilang").val();
    $.ajax({
      type : "POST",
      url : "{{ route('terbilang.process') }}",
      data : { '_token' : "{{ csrf_token() }}", input : input },
      success : function(result){
        var parse = $.parseJSON(result);
        var value = parse['value'];
        $(".angka_show").text(value);
      }
    });
    
  });
});
</script>

@endsection