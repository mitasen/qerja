<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\TerbilangController;

class TerbilangTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTerbilang()
    {
        $this->call('POST', '/terbilang',['input' => 'tiga ratus lima puluh satu juta delapan ribu sembilan ratus empat'])
            -> assertJson(['valid' => true]);
    }
}
